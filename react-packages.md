# React packages

## Utils
* [react-to-print](https://www.npmjs.com/package/react-to-print): Print react components

## State management
* [recoil](https://recoiljs.org/): 
* [joti](https://jotai.org/): Takes an atomic approach to global react state management.

## Icons
* [mdi-material-ui](https://www.npmjs.com/package/mdi-material-ui): SVG Icons.