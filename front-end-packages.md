# General Frontend packages

## Utils
* @heroicons: icons for [react](https://www.npmjs.com/package/@heroicons/react) and [vue](https://www.npmjs.com/package/@heroicons/vue)
* [valtio](https://www.npmjs.com/package/valtio): proxy-state

## Frontend Library
* [foundation](https://www.npmjs.com/package/foundation-sites)
