# Nodejs Packages

## Utils
* [event-loop-stats](https://www.npmjs.com/package/event-loop-stats)
* [pidusage](https://www.npmjs.com/package/pidusage): Process usage stats.
* [Luxon](https://www.npmjs.com/package/luxon) an alternative to moment.js
* [async](https://caolan.github.io/async/v3/): A module with powerful utility for working with asynchronous js.
* [rxjs][https://www.npmjs.com/package/rxjs]: A modular set of libraries to compose asynchronous and event-based programs using observable and compositions in JavaScript.
* [lodash](https://lodash.com/): A utility library that makes javascript easier by taking the hassle out of working with arrays, numbers, objects, strings etc.
* [underscore](https://underscorejs.org/): A utility belt library for javascript that provides support for the usual function aspects without extending any core javascript objects.
* [ramda](https://ramdajs.com/): A practical, functional library with side-effect free functions composable with currying.

## Testing
* [sigh-non](https://www.npmjs.com/package/sinon): Standalone test spies, stubs and mocks for javascript.

## Database
* [postgrator](https://www.npmjs.com/package/postgrator): Migration library using a directory of plain SQL scripts.

## Others
* [degit](https://www.npmjs.com/package/degit): Make copies of repos without the history.